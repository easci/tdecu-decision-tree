<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Mockery\Matcher\Any;
use PhpParser\Node\Expr\Array_;

class RegistrationMessage extends Notification
{
    use Queueable;

    protected $registration;
    

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $registration)
    {
        $this->registration = $registration;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $name = $this->registration['first_name'] . ' ' . $this->registration['last_name'];

        return (new MailMessage)
            ->subject( $name . ' just registered')
            ->text('A new user has registered.')
            ->from(config('admin.email'), config('admin.name'));

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
