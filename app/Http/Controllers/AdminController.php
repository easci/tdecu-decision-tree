<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Registration;
use App\Exports\ConsentExport;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export() 
    {
        return Excel::download(
            new ConsentExport, 'consent-' . date("Ymdhis") . '.xlsx'
        );
    }
}
