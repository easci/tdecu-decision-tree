<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConsentRequest;

use App\DecisionTree;
use App\Consent;

class DecisionController extends Controller
{
    /**
     * Display the index resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('/index');
    }

    /**
     * Display the welcome resource.
     *
     * @return \Redirect
     */
    public function realspace()
    {
        return \Redirect::to('http://1337.me/');
    }

    /**
     * Display the welcome resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function decision($id = 1)
    {
        // load the entire tree
        $nodes = DecisionTree::get();

        // verify url routing
        if ($id == 0 || $id > count($nodes)) {
            abort(404); 
        }

        // find the current node based on the url routing
        $node = $nodes->find($id);

        // decode json string stored in DB
        $node->content = json_decode($node->content);
        $node->options = json_decode($node->options);

        if ($node->form) {
            $node->form = json_decode($node->form);
        }

        return view('/decision', compact('node'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ConsentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function save(ConsentRequest $request)
    {
        Consent::create($request->all());

        // TODO: use Route::(...) handler instead
        return redirect($request->all()['submit_route']);
    }
}