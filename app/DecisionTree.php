<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class DecisionTree extends Model
{
    use NodeTrait;

    protected $primaryKey = 'decision_tree_id';
}
