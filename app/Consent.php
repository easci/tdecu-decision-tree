<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consent extends Model
{
    protected $table = 'sms_consent';
    protected $primaryKey = 'sms_consent_id';

	public static function create($data) {

		$consent = new Consent;

		$consent->first_name = $data['first_name'];
		unset($data['first_name']);

		$consent->last_name = $data['last_name'];
		unset($data['last_name']);

		$consent->phone_number = $data['phone_number'];
		unset($data['phone_number']);

		$consent->consent = ($data['consent'] === "on") ? true : false;
		unset($data['consent']);

		return $consent->save();
	}
}
