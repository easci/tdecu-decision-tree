<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $table = 'registrations';
    protected $primaryKey = 'registration_id';

	public static function create($data){

		$reg = new Registration;

		// Add standard Columns and remove them from data
		$reg->first_name = $data['first_name'];
		unset($data['first_name']);
		$reg->last_name = $data['last_name'];
		unset($data['last_name']);
		$reg->email = $data['email'];
		unset($data['email']);

		// Set the meta
		$reg->registration_meta = json_encode($data);

		return $reg->save();
	}
}
