<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

use App\Consent;

class ConsentExport implements FromCollection, WithMapping, WithHeadings
{

	private $headings = [
		"first_name" => "First Name",
		"last_name" => "Last Name",
		"created_at" => "Created at",
		"updated_at" => "Updated at",
		"phone_number" => "Phone #"
	];

    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
    	return Consent::all();
    }

    public function map($consent): array
    {
    	$consent = $consent->toArray();
    	$ret = [];

		foreach ($this->headings as $column_key => $heading) {
    		$ret[] = $consent[$column_key] ?? "";
		}

		return $ret;
    }

    public function headings(): array
    {
        return array_values($this->headings);
    }

    private function cleanColumn($data)
    {
    	if(is_array($data)){
    		return join(",", $data);
    	} else {
    		return $data;
    	}
    }
}
