<?php

// Decision
Route::get('/', 'DecisionController@index');

Route::get('/decision/{id}', 'DecisionController@decision')
    ->name('decision');

Route::post('/decision/save', 'DecisionController@save')
    ->name('decision.save');

Route::get('/realspace', 'DecisionController@realspace')
    ->name('realspace');

// Admin
Auth::routes(['verify' => true]);

Route::get('/admin/export', 'AdminController@export')
    ->name('admin.export')
    ->middleware('verified');

Route::view('/admin', 'admin.index')
    ->name('admin')
    ->middleware('verified');
