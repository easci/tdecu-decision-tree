@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<div class="column-row">
  <div class='column one-half'>
    <label>
      <div class='label'>First name</div>
      {{ Form::text('first_name', null, ['class' => 'form-control full-width']) }}
    </label>
  </div>

  <div class='column one-half'>
    <label>
      <div class='label'>Last name</div>
      {{ Form::text('last_name', null, ['class' => 'form-control full-width']) }}
    </label>
  </div>
</div>


<div class="column-row">
  <div class='column full'>
    <label>
      <div class='label'>Phone #</div>
      {{ Form::text('phone_number', null, ['class' => 'form-control full-width large']) }}
    </label>
  </div>
</div>

<div class="column-row">
  <div class='column full'>
    <p class="disclaimer">
      Disclaimer: By giving TDECU my phone number and agreeing below, I understand that I am providing my express consent to TDECU to permit TDECU to send me marketing and other text messages, including text messages sent using an automatic telephone dialing system, at the wireless number provided below. I understand that my consent to receive these messages is not required as a condition of purchasing any goods or services from TDECU. Message and data rates may apply.
    </p>
    <label class='checkbox'>
      {{ Form::checkbox('consent', null, false, ['class' => 'form-control', 'id' => 'consent']) }}
      <div class='fake-check'></div>
      <div class='label'>I Agree</div>
    </label>
  </div>
</div>

{{ Form::hidden('submit_route', $submit_route) }}
