<div class='block-wrap block-admin-header less-padding block-white'>
  <div class='block-inner'>
    <div class='column-row'>
      <div class='column one-fourth'>
        <a class="admin-site-name" href="{{ url('/') }}">
          {{ config('app.name', 'Laravel') }}
        </a>
      </div>
      <div class='column three-fourths'>
        <ul class="admin-navbar">
          <!-- Authentication Links -->
          @guest
          <li>
            <a href="/login">{{ __('Login') }}</a>
          </li>
          <li>
            @if (Route::has('register'))
            <a class="nav-link" href="/register">{{ __('Register') }}</a>
            @endif
          </li>
          @else
          <li>
            <a href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
            </a>

            <form id="logout-form" action="/logout" method="POST" style="display: none;">
              @csrf
            </form>
          </li>
          @endguest
        </ul>
      </div>
    </div>
  </div>
</div>


