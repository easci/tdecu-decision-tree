<div class="block-wrap less-padding block-footer">
  <div class="block-inner">
    <nav class="footer-nav">
      <div class="footer-logo-wrap">
        <a href="http://www.tdecu.org">
          <img class='logo-gray' src="https://www.tdecu.org/images/assets/tdecu-logo.svg">
        </a>
      </div>
    </nav>
    <ul>
      <li>&copy; 2018 TDECU. All rights reserved.</li>
    </ul>
  </div>
</div>
