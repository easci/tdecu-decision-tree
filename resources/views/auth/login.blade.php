@extends('layouts.app')

@section('content')
  <div class='admin-login-form'>
    <h2>{{ __('Login') }}</h2>
    <form method="POST" action="/login">
      @csrf

      <div class='column-row'>
        <div class='column full'>
          <label for="email" class="{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class='label'>{{ __('Email Address') }}</div>
            <input id="email" type="email" class="full-width large" name="email" value="{{ old('email') }}" required>
            <div class='note'>
              @if ($errors->has('email'))
                {{ $errors->first('email') }}
              @endif
            </div>
          </label>
        </div>
      </div>

      <div class='column-row'>
        <div class='column full'>
          <label for="password" class="{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class='label'>{{ __('Password') }}</div>
            <input id="password" type="password" class="full-width large" name="password" value="{{ old('password') }}" required>
            <div class='note'>
              @if ($errors->has('password'))
                {{ $errors->first('password') }}
              @endif
            </div>
          </label>
        </div>
      </div>

      <div class='column-row'>
        <div class='column full'>
          <label class="checkbox">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <span class="fake-check"></span>
            <div class="label">
              {{ __('Remember me') }}
            </div>
          </label>
        </div>
      </div>

      <div class='column-row'>
        <div class='column full'>
          <div class='button-row align-right'>
            <a class="button ghost" href="{{ route('password.request') }}">
              {{ __('Forgot your password?') }}
            </a>
            <button type="submit">
                {{ __('Login') }}
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
@endsection
