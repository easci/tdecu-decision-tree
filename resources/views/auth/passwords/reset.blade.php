@extends('layouts.app')

@section('content')
  <div class='admin-login-form'>
    <h2>{{ __('Reset Password') }}</h2>

  <!-- <form method="POST" action="{{ route('password.update') }}"> -->
  <form method="POST" action="/password/reset">
      @csrf

      <input type="hidden" name="token" value="{{ $token }}">

      <div class='column-row'>
        <div class='column full'>
          <label for="email" class="{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class='label'>{{ __('Email address') }}</div>
            <input id="email" type="email" class="full-width large" name="email" value="{{ old('email') }}" required>
            <div class='note'>
              @if ($errors->has('email'))
                {{ $errors->first('email') }}
              @endif
            </div>
          </label>
        </div>
      </div>

      <div class='column-row'>
        <div class='column full'>
          <label for="password" class="{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class='label'>{{ __('Password') }}</div>
            <input id="password" type="password" class="full-width large" name="password" value="{{ old('password') }}" required>
          </label>
        </div>
      </div>

      <div class='column-row'>
        <div class='column full'>
          <label for="password-confirm" class="{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class='label'>{{ __('Confirm password') }}</div>
            <input id="password-confirm" type="password" class="full-width large" name="password_confirmation" value="{{ old('password-confirm') }}" required>
            <div class='note'>
              @if ($errors->has('password'))
                {{ $errors->first('password') }}
              @endif
            </div>
          </label>
        </div>
      </div>

      <div class='column-row'>
        <div class='column full'>
          <div class='button-row align-right'>
            <button type="submit">
                {{ __('Reset password') }}
            </button>
          </div>
        </div>
      </div>
  </form>
</div>
@endsection
