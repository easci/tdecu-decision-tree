@extends('layouts.app')

@section('content')
<div class='admin-login-form'>
  <h2>{{ __('Reset Password') }}</h2>

  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif

  <form method="POST" action="/password/email">
    @csrf

    <div class='column-row'>
      <div class='column full'>
        <label for="email" class="{{ $errors->has('email') ? ' has-error' : '' }}">
          <div class='label'>{{ __('Email address') }}</div>
          <input id="email" type="email" class="full-width large" name="email" value="{{ old('email') }}" required>
          <div class='note'>
            @if ($errors->has('email'))
              {{ $errors->first('email') }}
            @endif
          </div>
        </label>
      </div>
    </div>

    <div class='column-row'>
      <div class='column full'>
        <div class='button-row align-right'>
          <button type="submit">
            {{ __('Send password reset link') }}
          </button>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
