@extends('layouts.app')

@section('content')
<div class="admin-login-form">
  <h2>{{ __('Verify Your Email Address') }}</h2>

  @if (session('resent'))
  <div class="alert alert-success" role="alert">
    <p>{{ __('A fresh verification link has been sent to your email address.') }}</p>
  </div>
  @endif

  <p>{{ __('Before proceeding, please check your email for a verification link.') }}</p>
  <p>{{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.</p>
</div>
@endsection
