<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('shared.head')
  </head>
  <body>
    <div id='app'>

      <div class='arrow-flair'></div>

      @if(Request::is('admin/*'))
        @include('shared.navbar')
      @endif

      @yield('content')

      @include('shared.footer')
    </div>
  </body>
</html>
