@extends('layouts.app')

@section('content')
  <div class='block-wrap block-hero block-decision more-top-padding block-grey-9'>
    <div class='block-inner text-container'>
      <img src='/img/tdecu-logo-red.svg' class='logo' />
      <div class='column-row decision-container'>
        <div class='column one-half'>
          <h2 class='step'>{{$node->title}}</h2>


          @foreach ($node->content as $block)
            @if (is_object($block))
              <p class='{{$block->class}}'>{{ $block->text}}</p>
            @else
              <p>{{ $block }}</p>
            @endif
          @endforeach

          @if ($node->form)
            {{ Form::open(['route' => $node->form->save_route, 'class' => 'some-class']) }}
              @include($node->form->partial, ['submit_route' => $node->form->submit_route])
              <div class='button-row align-right'>
                @foreach ($node->options as $option)
                  @if (isset($option->type) && $option->type === 'submit')
                    <button type='submit' class="button {{ isset($option->extra_class) ? $option->extra_class : ''}}">
                      {{$option->text}}</button>
                  @else
                    <a class="button {{ isset($option->extra_class) ? $option->extra_class : ''}}" 
                          href='{{$option->link}}'>
                        {{$option->text}}</a>
                  @endif
                @endforeach
              </div>
            {{ Form::close() }}
          @else
            <div class='button-row align-right'>
              @foreach ($node->options as $option)
                <a class="button {{ isset($option->extra_class) ? $option->extra_class : ''}}" 
                    href='{{$option->link}}'>
                  {{$option->text}}</a>
              @endforeach
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
@stop
