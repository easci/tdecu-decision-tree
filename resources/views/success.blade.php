<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Farm Beats Registration</title>
    <link href="/css/app.css" rel='stylesheet' />
    <style>
      select { width: 100%; }
    </style>
  </head>
  <body>
    <div class='page-container'>
      <div class='block-wrap block-hero block-hero-detail max-top-padding block-photo-bg' style='background-image: url(/img/farm-beats-header.jpg)'>
        <img src="/img/msft-logo-gray.png" class='msft-logo' />
        <img src="/img/precision-ag-conference.png" class='partner-logo' />
        <div class='block-inner text-container'>
          <div class='header'>
            <h1>IoT For Earth Summit</h1>
            <h2>Jaunary 14, 2019 <span>12:00PM - 7:30PM</span></h2>
            <h3>Prior to Precision Ag Vision Conference</h3>
          </div>

          <div class='column-row'>
            <div class='column two-thirds'>
              <p>Unlock the transformative potential of intelligent edge and
              intelligent cloud for your agriculture business.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    @include('shared.footer') 
  </body>
</html>
