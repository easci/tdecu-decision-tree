@extends('layouts.app')

@section('content')
  <div class='block-wrap block-grey-9 block-hero more-top-padding'
      style='background-image: url()'>
    <div class='block-inner text-container'>
      <div class='column-row'>
        <div class='column one-half'>
          <img src="/img/tdecu-logo-red.svg" class='logo' />
          <h2>The new Forward Mobile U release is coming soon!</h2>
          <h3>Follow these instructions to prepare. It takes only a few minutes.</h3>

          <div class='column-row'>
            <div class='column two-thirds'>
              <a class='button large' href="{{ route('decision', 1) }}">
                Get started
              </a>
            </div>
          </div>
        </div>
        <div class='column one-half'>
          <img class='iphone-mockup' src="/img/iphone.png" />
        </div>
      </div>
    </div>
  </div>
@stop
