@extends('layouts.app')

@section('content')
  @if (session('status'))
      <div class="alert alert-success" role="alert">
          {{ session('status') }}
      </div>
  @endif
  @guest
      <h2>Login to access admin. If you email is not accepted please reach out to <a href="mailto:jens@easci.com">jens@easci.com</a> to have it added.</h2>
  @else
      @include('admin-export')
  @endguest
@endsection
