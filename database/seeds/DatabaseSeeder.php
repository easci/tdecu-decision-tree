<?php

use Illuminate\Database\Seeder;
use App\DecisionTree;
use function GuzzleHttp\json_encode;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        // rebuild the tree every seed
        DB::table('decision_trees')->truncate();

        $node = DecisionTree::create([  
            // 1
            'slug' => 'step-1',
            'title' => '1',
            'content' => json_encode(array(
                'The first step to preparing for the new release is to delete any previous versions of Forward Mobile U you may have.'
            )),
            'options' => json_encode(array(
                array(
                    'link' => '/decision/2',
                    'text' => 'I don\'t have it or I\'ve deleted it'
                ),
            )),
            'children' => [
                [ // 2
                    'slug' => 'step-2',
                    'title' => '2',
                    'content' => json_encode(array(
                        'Do you have a TDECU-issued mobile device (i.e. "a work phone")?'
                    )),
                    'options' => json_encode(array(
                        array(
                            'link' => '/decision/3',
                            'text' => 'No',
                        ),
                        array(
                            'link' => '/decision/6',
                            'text' => 'Yes'
                        )
                    )),
                    'children' => [
                        [ // 3
                            'slug' => 'step-3-no',
                            'title' => '3',
                            'content' => json_encode(array(
                                'No problem! The Forward Mobile U app will be available in the Google Play and Apple App Store with the next release.',
                                'Would you like to sign up to receive an SMS text reminder when the app is available so you can easily download it?'
                            )),
                            'form' => json_encode(array(
                                'model' => 'Consent',
                                'partial' => 'shared/consent-form',
                                'save_route' => 'decision.save',
                                'submit_route' => '/decision/5'
                            )),
                            'options' => json_encode(array(
                                array(
                                    'link' => '/decision/4',
                                    'text' => 'No thanks',
                                    'extra_class' => 'ghost'
                                ),
                                array(
                                    'type' => 'submit',
                                    'link' => '/decision/5',
                                    'text' => 'Submit'
                                )
                            )),
                            'children' => [
                                [ // 4
                                    'slug' => 'step-4-phone',
                                    'title' => '4',
                                    'content' => json_encode(array(
                                        'No problem. We\'ll notify you via email and on REAL Space when the app is ready to download!',
                                        'At that time you\'ll simply visit the Apple App Store or Google Play Store and search "Forward Mobile U" and download as you would any other app.',
                                        'Be sure to watch REAL Space for more news about the Digital Development project and to win cool prizes!'
                                    )),
                                    'options' => json_encode(array(
                                        array(
                                            'link' => '/realspace',
                                            'text' => 'Take me back to REAL Space',
                                            'extra_class' => 'large'
                                        )
                                    ))
                                ],
                                [ // 5
                                    'slug' => 'step-4-no-phone',
                                    'title' => '4',
                                    'content' => json_encode(array(
                                        'Great, we\'ll text you a download link once the app is available.',
                                        'Be sure to watch REAL Space for more news about the Digital Development project and to win cool prizes!'
                                    )),
                                    'options' => json_encode(array(
                                        array(
                                            'link' => '/realspace',
                                            'text' => 'Take me back to REAL Space',
                                            'extra_class' => 'large'
                                        )
                                    ))
                                ]
                            ]
                        ],
                        [ // 6
                            'slug' => 'step-3-yes',
                            'title' => '3',
                            'content' => json_encode(array(
                                'Great! Your setup is complete.',
                                'You will receive an SMS text message on your work mobile phone when Forward Mobile U is ready to download fro the App Store.',
                                'Be sure to watch REAL Space for more news about the Digital Development project and to win cool prizes!'
                            )),
                            'options' => json_encode(array(
                                array(
                                    'link' => '/realspace',
                                    'text' => 'Take me back to REAL Space',
                                    'extra_class' => 'large'
                                )
                            ))
                        ]
                    ],
                ],
            ],
        ]);
   }
}
