<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDecisionTreesTable extends Migration
{
    public function up()
    {
        Schema::create('decision_trees', function (Blueprint $table) {
            $table->increments('decision_tree_id');
            $table->string('slug');
            $table->string('title');
            $table->json('content')->nullable();
            $table->json('form')->nullable();
            $table->json('options')->nullable();
            $table->timestamps();
            $table->nestedSet();
        });
    }

    public function down()
    {
        Schema::dropIfExists('decision_trees');
    }
}
