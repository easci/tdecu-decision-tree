<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsConsentTable extends Migration
{
    public function up()
    {
        Schema::create('sms_consent', function (Blueprint $table) {
            $table->increments('sms_consent_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone_number');
            $table->boolean('consent');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sms_consent');
    }
}
