# Farm Beats

##install dependencies

```sh
npm install
composer install
php artisan key:generate
```

## Create .env

```sh
cp .env.example .env
```

fill out db and other credentials

## Generate project key

```sh
php artisan key:generate
```

## starting the server

```sh
php artisan serve
```

## building and watching

one time build

```sh
npm run dev
```

build with watch
```sh
npm run watch
```

## Setting up a local database

```sh
docker run --name=farm_beats -d -e MYSQL_ROOT_HOST="172.0.0.1" -e MYSQL_DATABASE=farm_beats_local -e MYSQL_USER=farm_beats_local -e MYSQL_PASSWORD=root -p 3306 mysql/mysql-server:5.7
```

run

```sh
docker ps
```

find the image to then see the port to connect to it should be under the ports column as YOUR_PORT->3306/tcp

### Credentials

host: localhost
db: farm_beats_local
port: 3306
user: farm_beats_local
password: root

Note: make sure to but these credentials in your local env